console.log("Welcome to magic notes");

showNotes();

// if user adds a note , add it to the local storage 

let addBtn = document.getElementById('addBtn');

addBtn.addEventListener('click',function(e){

    let addTxt = document.getElementById('addTxt');

    let notes = localStorage.getItem("notes");
    if(notes == null){
        notesObj = [];
    }
    else{
        notesObj = JSON.parse(notes);
    }

    notesObj.push(addTxt.value);
    localStorage.setItem("notes",JSON.stringify(notesObj));
    addTxt.value = "";

    console.log(notesObj);

    showNotes();
})


// Function to show localStorage
function showNotes(){
    let notes = localStorage.getItem("notes");
    if(notes == null){
        notesObj = [];
    }
    else{
        notesObj = JSON.parse(notes);
    }

    let html = "" ;  // blank string
    notesObj.forEach(function(element, index){
        html += `
        
        <div class="card noteCard mx-2 my-2" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">Work ${index + 1}</h5>
          <p class="card-text">${element}</p>
          <button id="${index}" onclick="deleteNote(this.id)" class="btn btn-primary">Delete Work</button>
        </div>
      </div>

        `
    });

    let notesElm = document.getElementById("notes");  // div 
    if(notesObj.length != 0){
        notesElm.innerHTML = html;
    }
    else{
        notesElm.innerHTML = `Nothing to Show : Please add`;
    }
}


// function to delete a note

// array ki index pass karenge , jis postion ko delete krna hai
function deleteNote(index){
    console.log("i'm deleting",index);
    let notes = localStorage.getItem("notes");
    if(notes == null){
        notesObj = [];
    }
    else{
        notesObj = JSON.parse(notes);
    }

    // splice strting leta hai , or ye puccta hai ki vaha se kitni position tak k elements ko delete krna hai  
    notesObj.splice(index,1);
    localStorage.setItem("notes",JSON.stringify(notesObj));
    showNotes();            
}



//// searching

let search = document.getElementById('searchTxt');
search.addEventListener('input',function(){

    let inputVal = search.value;
    console.log("input event fired");

    let noteCards = document.getElementsByClassName("noteCard");     /// noteCard class present in this javascript page (in div) 

    Array.from(noteCards).forEach(function(element){
        let cardTxt = element.getElementsByTagName("p")[0].innerText;
        if(cardTxt.includes(inputVal)){
            element.style.display = "block";
        }
        else{
            element.style.display = "none";
        }
    })

})
